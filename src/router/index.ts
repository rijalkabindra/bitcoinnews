import Vue from 'vue';
import VueRouter, {RouteConfig} from 'vue-router';
import Home from '../views/Home.vue';
import Weathers from '@/views/Weathers.vue';
import store from '@/store/store';

Vue.use(VueRouter);

class RouteMeta {

  title: string;

  constructor({title}: { title: string }) {
    this.title = title;
  }
}

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: new RouteMeta({title: 'Bitcoin News'}),
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    meta: new RouteMeta({title: 'About'}),
  },
  {
    path: '/weather',
    name: 'Weather',
    component: Weathers,
    meta: new RouteMeta({title: 'Weather'}),
  },
];



const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const routeMeta = to.meta as RouteMeta;
  store.dispatch('topToolbar/changeTitle', routeMeta.title);
  next();
});

export default router;
