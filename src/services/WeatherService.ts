import axios from 'axios';
import {Weather} from '@/types';
import {WeatherEntity} from '@/types';

class WeatherService {
  public async getWeather(): Promise<Weather> {
    return axios.get('https://api.openweathermap.org/data/2.5/weather?q=kathmandu&appid=aac0f6b582859e7a4742ea0a11ac066a');
  }
}

export default new WeatherService();
