import {News} from '@/types';
import axios from 'axios';

class NewsServices {

  public async getNews(): Promise<News> {
    return axios.get('https://newsapi.org/v2/everything?q=bitcoin&apiKey=b50375d804f64772bd54028e86ea1ddd');
  }
}

export default new NewsServices();
