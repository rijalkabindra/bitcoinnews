import Vue from 'vue';
import Vuex from 'vuex';
import NewsServices from '@/services/NewsServices';
import WeatherService from '@/services/WeatherService';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    news: {},
    weather: {},
  },
  mutations: {
    setNews(state, data) {
      state.news = data.data;
    },
    setWeather(state, data){
      state.weather = data.data;
    },
  },
  actions: {
    getNews: ({commit}) => {
      NewsServices.getNews().then((res: any) => {
        commit('setNews', res);
      }).catch((err: any) => {
        console.log(err.message);
      });
    },
    getWeather: ({commit}) => {
      WeatherService.getWeather().then((res: any) => {
        console.log(res)
        commit('setWeather', res);
      }).catch((err: any) => {
        console.log(err.message);
      });
    },
  },
  modules: {},
});
