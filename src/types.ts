export interface Paths {
  title: string;
  icon: string;
  path: string;
}

export interface News {
  status: string;
  totalResults: number;
  articles?: (ArticlesEntity)[] | null;
}

export interface ArticlesEntity {
  source: Source;
  author?: string | null;
  title: string;
  description: string;
  url: string;
  urlToImage: string;
  publishedAt: string;
  content?: string | null;
}

export interface Source {
  id?: string | null;
  name: string;
}


//Weather API
export interface Weather {
  coord: Coord;
  weather?: (WeatherEntity)[] | null;
  base: string;
  main: Main;
  wind: Wind;
  clouds: Clouds;
  dt: number;
  sys: Sys;
  id: number;
  name: string;
  cod: number;
}

export interface Coord {
  lon: number;
  lat: number;
}

export interface WeatherEntity {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface Main {
  temp: number;
  pressure: number;
  humidity: number;
  temp_min: number;
  temp_max: number;
}

export interface Wind {
  speed: number;
  deg: number;
}

export interface Clouds {
  all: number;
}

export interface Sys {
  type: number;
  id: number;
  message: number;
  country: string;
  sunrise: number;
  sunset: number;
}

export interface RootState{
  topToolbar: TopToolbarState;
}


export interface TopToolbarState{
  title: string;
}

